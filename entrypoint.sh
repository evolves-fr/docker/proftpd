#!/bin/sh

for i in $USERS ; do
    NAME=$(echo $i | cut -d'|' -f1)
    PASS=$(echo $i | cut -d'|' -f2)
  FOLDER=$(echo $i | cut -d'|' -f3)
     UID=$(echo $i | cut -d'|' -f4)

  echo "Create user $NAME"

  if [ -z "$FOLDER" ]; then
    FOLDER="/ftp/$NAME"
  fi

  if [ ! -z "$UID" ]; then
    UID_OPT="--uid $UID"
  fi

  mkdir -p $(dirname "$FOLDER")
  useradd --create-home --home-dir $FOLDER --password $(echo $PASS | openssl passwd -1 -stdin) --shell /usr/sbin/nologin $UID_OPT $NAME
  unset NAME PASS FOLDER UID
done

echo "Generate configuration"
envsubst < /usr/local/share/proftpd/templates/proftpd.conf > /etc/proftpd/proftpd.conf

# Used to run custom commands inside container
if [ ! -z "$1" ]; then
  echo "Exec $@"
  exec "$@"
else
  echo "Starting proftpd"
  exec /usr/sbin/proftpd --nodaemon --config /etc/proftpd/proftpd.conf
fi
