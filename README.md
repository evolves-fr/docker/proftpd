# ProFTPd

## Configuration

| Variable                          | Description                       | Default                                                                       |
|-----------------------------------|-----------------------------------|-------------------------------------------------------------------------------|
| `USERS`                           |                                   | ``                                                                            |
| `USE_IPV6`                        |                                   | `on`                                                                          |
| `IDENT_LOOKUPS`                   |                                   | `off`                                                                         |
| `SERVER_NAME`                     |                                   | `ProFTPd in Docker`                                                           |
| `SERVER_TYPE`                     |                                   | `standalone`                                                                  |
| `DEFER_WELCOME`                   |                                   | `off`                                                                         |
| `MULTILINE_RFC2228`               |                                   | `on`                                                                          |
| `DEFAULT_SERVER`                  |                                   | `on`                                                                          |
| `SHOW_SYMLINKS`                   |                                   | `on`                                                                          |
| `TIMEOUT_NO_TRANSFER`             |                                   | `600`                                                                         |
| `TIMEOUT_STALLED`                 |                                   | `600`                                                                         |
| `TIMEOUT_IDLE`                    |                                   | `1200`                                                                        |
| `DISPLAY_LOGIN`                   |                                   | `welcome.msg`                                                                 |
| `DISPLAY_CHDIR`                   |                                   | `.message true`                                                               |
| `LIST_OPTIONS`                    |                                   | `-l`                                                                          |
| `DENY_FILTER`                     |                                   | `\*.*/`                                                                       |
| `DEFAULT_ROOT`                    |                                   | `~`                                                                           |
| `REQUIRE_VALID_SHELL`             |                                   | `on`                                                                          |
| `PORT`                            |                                   | `21`                                                                          |
| `MIN_PASSIVE_PORT`                |                                   | `30000`                                                                       |
| `MAX_PASSIVE_PORT`                |                                   | `30009`                                                                       |
| `MASQUERADE_ADDRESS`              |                                   | ``                                                                            |
| `MAX_INSTANCES`                   |                                   | `30`                                                                          |
| `USER`                            |                                   | `proftpd`                                                                     |
| `GROUP`                           |                                   | `on`                                                                          |
| `REQUIRE_VALID_SHELL`             |                                   | `nogroup`                                                                     |
| `UMASK_FILE`                      |                                   | `022`                                                                         |
| `UMASK_DIRECTORY`                 |                                   | `022`                                                                         |
| `ALLOW_OVERWRITE`                 |                                   | `on`                                                                          |
| `PERSISTENT_PASSWD`               |                                   | `off`                                                                         |
| `AUTH_ORDER`                      |                                   | `mod_auth_pam.c* mod_auth_unix.c`                                             |
| `USE_SEND_FILE`                   |                                   | `off`                                                                         |
| `TRANSFER_LOG`                    |                                   | `/var/log/proftpd/xferlog`                                                    |
| `SYSTEM_LOG`                      |                                   | `/var/log/proftpd/proftpd.log`                                                |
| `TLS_ENGINE`                      |                                   | `off`                                                                         |
| `TLS_LOG`                         |                                   | `/var/log/proftpd/tls.log`                                                    |
| `TLS_PROTOCOL`                    |                                   | `SSLv23`                                                                      |
| `TLS_RSA_CERTIFICATE_FILE`        |                                   | ``                                                                            |
| `TLS_RSA_CERTIFICATE_KEY_FILE`    |                                   | ``                                                                            |
| `TLS_CA_CERTIFICATE_FILE`         |                                   | ``                                                                            |
| `TLS_CA_CERTIFICATE_PATH`         |                                   | `/etc/ssl/certs`                                                              |
| `TLS_OPTIONS`                     |                                   | `NoCertRequest EnableDiags NoSessionReuseRequired AllowClientRenegotiations`  |
| `TLS_VERIFY_CLIENT`               |                                   | `off`                                                                         |
| `TLS_REQUIRED`                    |                                   | `on`                                                                          |
| `TLS_RENEGOTIATE_GLOBAL`          |                                   | `required`                                                                    |
| `TLS_RENEGOTIATE_VIRTUAL_HOST`    |                                   | `off`                                                                         |

## Versions

| Docker            | proftpd-basic     | openssl           | gettext           | ca-certificates   |
|-------------------|-------------------|-------------------|-------------------|-------------------|
| 1.0.0             | 1.3.6-4+deb10u5   | 1.1.1d-0+deb10u6  | 0.19.8.1-9        | 20200601~deb10u2  |

## Developer

### Structure

```
/
|-- run
|	`-- proftpd
|-- usr
|	|-- sbin
|	|	`-- proftpd
|	|-- lib
|	|	`-- proftpd
|   |       |-- mod_auth_otp.so
|   |       |-- mod_ban.so
|   |       |-- mod_copy.so
|   |       |-- mod_ctrls_admin.so
|   |       |-- mod_deflate.so
|   |       |-- mod_dynmasq.so
|   |       |-- mod_exec.so
|   |       |-- mod_facl.so
|   |       |-- mod_ifsession.so
|   |       |-- mod_ifversion.so
|   |       |-- mod_load.so
|   |       |-- mod_memcache.so
|   |       |-- mod_quotatab.so
|   |       |-- mod_quotatab_file.so
|   |       |-- mod_quotatab_radius.so
|   |       |-- mod_quotatab_sql.so
|   |       |-- mod_radius.so
|   |       |-- mod_ratio.so
|   |       |-- mod_readme.so
|   |       |-- mod_redis.so
|   |       |-- mod_rewrite.so
|   |       |-- mod_sftp.so
|   |       |-- mod_sftp_pam.so
|   |       |-- mod_sftp_sql.so
|   |       |-- mod_shaper.so
|   |       |-- mod_site_misc.so
|   |       |-- mod_sql.so
|   |       |-- mod_sql_passwd.so
|   |       |-- mod_tls.so
|   |       |-- mod_tls_memcache.so
|   |       |-- mod_tls_redis.so
|   |       |-- mod_unique_id.so
|   |       |-- mod_wrap.so
|   |       |-- mod_wrap2.so
|   |       |-- mod_wrap2_file.so
|   |       |-- mod_wrap2_redis.so
|   |       `-- mod_wrap2_sql.so
|   |-- local
|   |   `-- share
|   |       `-- proftpd
|   |           `-- templates
|   |               `-- proftpd.conf
|	`-- share
|		`-- proftpd
|			`-- templates
|				|-- geoip.conf
|				|-- ldap.conf
|				|-- mod_snmp.conf
|				|-- modules.conf
|				|-- proftpd.conf
|				|-- sql.conf
|				|-- tls.conf
|				|-- virtuals.conf
|				`-- welcome.msg
|-- var
|	`-- log
|		`-- proftpd
`-- etc
    |-- pam.d
    |	`-- proftpd
    |-- init.d
    |	`-- proftpd
    |-- default
    |	`-- proftpd
    `-- proftpd
        |-- blacklist.dat
        |-- conf.d
        |-- dhparams.pem
        |-- ldap.conf
        |-- modules.conf
        |-- proftpd.conf
        |-- sql.conf
        |-- tls.conf
        `-- virtuals.conf
```


## Sources

- http://www.proftpd.org/
- http://www.proftpd.org/docs/
- http://www.proftpd.org/docs/directives/configuration_full.html
- http://www.proftpd.org/docs/howto/Chroot.html
- https://github.com/proftpd/proftpd
- https://doc.ubuntu-fr.org/proftpd





