FROM debian:10.10

ARG VERSION
ARG COMMIT

LABEL org.opencontainers.image.title="proftpd" \
      org.opencontainers.image.description="Highly configurable GPL-licensed FTP server software" \
      org.opencontainers.image.vendor="EVOLVES™ SAS" \
      org.opencontainers.image.authors="Thomas FOURNIER <tfournier@evolves.fr>" \
      org.opencontainers.image.source="https://gitlab.com/evolves-fr/docker/vsftpd" \
      org.opencontainers.image.version="$VERSION" \
      org.opencontainers.image.revision="$COMMIT" \
      org.opencontainers.image.licenses="GPL-2.0"

RUN apt update && apt install -y \
    proftpd-basic=1.3.6-4+deb10u5 \
    openssl=1.1.1d-0+deb10u6 \
    gettext=0.19.8.1-9 \
    ca-certificates=20200601~deb10u2 \
    && rm -rf /var/lib/apt/lists/*

ENV USE_IPV6="on" \
    IDENT_LOOKUPS="off" \
    SERVER_NAME="ProFTPd in Docker" \
    SERVER_TYPE="standalone" \
    DEFER_WELCOME="off" \
    MULTILINE_RFC2228="on" \
    DEFAULT_SERVER="on" \
    SHOW_SYMLINKS="on" \
    TIMEOUT_NO_TRANSFER="600" \
    TIMEOUT_STALLED="600" \
    TIMEOUT_IDLE="1200" \
    DISPLAY_LOGIN="welcome.msg" \
    DISPLAY_CHDIR=".message true" \
    LIST_OPTIONS="-l" \
    DENY_FILTER="\*.*/" \
    DEFAULT_ROOT="~" \
    REQUIRE_VALID_SHELL="on" \
    PORT="21" \
    MIN_PASSIVE_PORT="30000" \
    MAX_PASSIVE_PORT="30009" \
    MASQUERADE_ADDRESS="" \
    MAX_INSTANCES="30" \
    USER="proftpd" \
    GROUP="nogroup" \
    UMASK_FILE="022" \
    UMASK_DIRECTORY="022" \
    ALLOW_OVERWRITE="on" \
    PERSISTENT_PASSWD="off" \
    AUTH_ORDER="mod_auth_pam.c* mod_auth_unix.c" \
    USE_SEND_FILE="off" \
    TRANSFER_LOG="/var/log/proftpd/xferlog" \
    SYSTEM_LOG="/var/log/proftpd/proftpd.log" \
    TLS_ENGINE="off" \
    TLS_LOG="/var/log/proftpd/tls.log" \
    TLS_PROTOCOL="SSLv23" \
    TLS_RSA_CERTIFICATE_FILE="" \
    TLS_RSA_CERTIFICATE_KEY_FILE="" \
    TLS_CA_CERTIFICATE_FILE="" \
    TLS_CA_CERTIFICATE_PATH="/etc/ssl/certs" \
    TLS_OPTIONS="NoCertRequest EnableDiags NoSessionReuseRequired AllowClientRenegotiations" \
    TLS_VERIFY_CLIENT="off" \
    TLS_REQUIRED="on" \
    TLS_RENEGOTIATE_GLOBAL="required" \
    TLS_RENEGOTIATE_VIRTUAL_HOST="off"

RUN echo "/usr/sbin/nologin" >> /etc/shells

COPY entrypoint.sh /usr/local/bin/entrypoint
COPY proftpd.conf /usr/local/share/proftpd/templates/proftpd.conf

EXPOSE 21 30000-30009
ENTRYPOINT ["/usr/local/bin/entrypoint"]
